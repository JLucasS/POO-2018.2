package br.ucsal.bes20182.poo.atividade03;

import java.util.Scanner;

public class Atividade03 {

	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		//Declaração de Varivel
		int[] num = new int[5];
		
		//Saída
		obterNumero(num);
		encontrarMaiorNumero(num);
		exibirMaiorNumero(num);

	}

	public static void obterNumero (int[] num) {
		
		//Entrada de Dados
		int i = 0;

		do {

			System.out.println("Informe um nmero");
			num[i] = sc.nextInt();
			i++;

		} while (i < 5);

	}
	
	public static int encontrarMaiorNumero(int[] num) {
		
		//Processamento de Dados
		int maior = 0;
		for (int i = 0; i < num.length; i++) {
			if (num[i] > maior) {
				maior = num[i];
			}
		}

		return maior;

	}
	
	public static void exibirMaiorNumero (int[] num) {
		
		//Processamento de Dados
		int a  = encontrarMaiorNumero(num);

		System.out.println("\n" + "Dentre os valores informados, o maior  " + a);


	}

}
