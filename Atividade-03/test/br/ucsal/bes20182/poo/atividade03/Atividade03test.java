package br.ucsal.bes20182.poo.atividade03;

import org.junit.Assert;

import org.junit.Test;

public class Atividade03test {
	@Test
	public void maiorNumeroTest() {
		
		int[] num = {4, 8, 7, 4, 3};
		
		int maiorNumero = Atividade03.encontrarMaiorNumero(num);
		
		int valorCorreto = 8;
		
		Assert.assertEquals(valorCorreto, maiorNumero);
		
		
	}

}
